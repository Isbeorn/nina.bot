module.exports = {
    $id: 'https://nighttime-imaging.eu/afgraphversion.schema.json',
    $schema: 'http://json-schema.org/draft-07/schema#',
    title: 'AF Graph Version',
    type: 'object',
    properties: {
        Version: {
            type: 'number'
        }
    }
};
